" source: https://www.vi-improved.org/recommendations/

" Add wildcard
nnoremap <leader>a :argadd <c-r>=fnameescape(expand('%:p:h'))<cr>/*<C-d>

" lands on buffer prompt and displays all buffers
nnoremap <leader>b :b <C-d>

" similar to buffers but for opening single file
nnoremap <leader>e :e **/

" drops to grep line
nnoremap <leader>g :grep<space>

" uses Ilist function from qlist
nnoremap <leader>i :Ilist<space>

" lands me on a taglist jump command line
nnoremap <leader>j :tjump /

" runs make 
nnoremap <leader>m :make<cr>
