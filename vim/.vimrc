"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Basic settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


syntax on
set noerrorbells
set confirm
set tabstop=4 softtabstop=4
set shiftwidth=4
set wildignore+=.pyc,.swp
set ruler
set expandtab
set smartindent
set relativenumber
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set autoread
set cursorline
set encoding=UTF-8
set transparency=5
set pastetoggle=<F2>
let mapleader=","


inoremap jk <ESC>
inoremap kj <ESC>

nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-l> :wincmd l<CR>

vnoremap < <gv
vnoremap > >gv


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Open file exactly where you left it
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if has("autocmd")
au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
\| exe "normal! g'\"" | endif
endif


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Highlight matching brackets
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set showmatch matchtime=3


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Others
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

source $HOME/.vim/config/netrw.vim
source $HOME/.vim/config/statusl.vim
source $HOME/.vim/config/files.vim
source $HOME/.vim/config/completion.vim
