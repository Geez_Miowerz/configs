" basics
filetype plugin indent on
syntax on set number
set relativenumber
set incsearch
set ignorecase
set smartcase
set nohlsearch
set tabstop=4
set softtabstop=0
set shiftwidth=4
set expandtab
set nobackup
set noswapfile
set splitbelow splitright
set inccommand=split

" Disable the fucking shitty paste (insert bs)
set nopaste

" Set a virtual line under the cursor's current line
set cursorline

" Removing autocompletion on comments
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Preferences
inoremap jk <ESC>
let mapleader = ","
set pastetoggle=<F2>

" Stay in visual mode when indenting. You will never have to run gv after
" performing an indentation.
vnoremap < <gv
vnoremap > >gv

" Make Y yank everything from the cursor to the end of the line. This makes Y
" act more like C or D because by default, Y yanks the current line (i.e. the
" same as yy).
noremap Y y$

" navigate split screens easily
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-l> :wincmd l<CR>

" change spacing for language specific
autocmd Filetype javascript setlocal ts=2 sts=2 sw=2

" Splits open at bottom and right
set splitbelow splitright

" Splitting Screen
map <C-h> <C-w>h
map <C-j> <C-w>k
map <C-k> <C-w>k
map <C-l> <C-w>l

" Open file exactly where you left it
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

" Share Clipboard
set clipboard+=unnamedplus

" Open netrw in vertical split
nnoremap <leader>v :Vex<CR><CR>

" Open netrw in horizontal split
nnoremap <leader>h :Hex<CR><CR>

" Change dir view
let g:netrw_liststyle = 3

" Remove banner
let g:netrw_banner = 0

" Set dir explorer width
let g:netrw_winsize = 25

" Open file in vertical split to the right (Has some issues, unable to resolve them)
augroup netrw_mappings
    autocmd!
    autocmd filetype netrw call Netrw_mappings()
augroup END
function! OpenToRight()
  :rightbelow vnew
  :wincmd p
  :normal P
endfunction
